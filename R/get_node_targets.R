get_node_targets <- function(x, originNode) {

  nodeIndices <- which(x$origins == originNode);

  targets <- x$targets[nodeIndices];

  return(targets);

}
