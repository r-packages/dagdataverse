
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <img src='man/figures/logo.png' align="right" style="height:250px" /> raven 📦

## raven helps to conduct DAG-based simulations

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/raven)](https://cran.r-project.org/package=raven)

[![Dependency
status](https://tinyverse.netlify.com/badge/raven)](https://CRAN.R-project.org/package=raven)

[![Pipeline
status](https://gitlab.com/r-packages/raven/badges/main/pipeline.svg)](https://gitlab.com/r-packages/raven/-/commits/main)

[![Downloads last
month](https://cranlogs.r-pkg.org/badges/last-month/raven?color=brightgreen)](https://cran.r-project.org/package=raven)

[![Total
downloads](https://cranlogs.r-pkg.org/badges/grand-total/raven?color=brightgreen)](https://cran.r-project.org/package=raven)

[![Coverage
status](https://codecov.io/gl/r-packages/raven/branch/main/graph/badge.svg)](https://app.codecov.io/gl/r-packages/raven?branch=main)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://raven.opens.science>. If the development version also has a
pkgdown website, that’s located at <https://r-packages.gitlab.io/raven>.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of `raven` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('raven');
```

You can install the development version of `raven` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/raven');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

You can install the cutting edge development version (own risk, don’t
try this at home, etc) of `raven` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/raven@dev');
```

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
