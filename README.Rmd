---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r setup, include=FALSE}

knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)

# install.packages('pkgnet');

packagename <- 'raven';
packageSubtitle <- "raven helps to conduct DAG-based simulations";

gitLab_ci_badge <-
  paste0("https://gitlab.com/r-packages/", packagename, "/badges/main/pipeline.svg");
gitLab_ci_url <-
  paste0("https://gitlab.com/r-packages/", packagename, "/-/commits/main");

codecov_badge <-
  paste0("https://codecov.io/gl/r-packages/", packagename, "/branch/main/graph/badge.svg");
codecov_url <-
  paste0("https://app.codecov.io/gl/r-packages/", packagename, "?branch=main");

dependency_badge <-
  paste0("https://tinyverse.netlify.com/badge/", packagename);
dependency_url <-
  paste0("https://CRAN.R-project.org/package=", packagename);

cran_badge <-
  paste0("https://www.r-pkg.org/badges/version/", packagename);
cran_url <-
  paste0("https://CRAN.R-project.org/package=", packagename);
cran_url <-
  paste0("https://cran.r-project.org/package=", packagename);
cranVersion_badge <-
  paste0("https://www.r-pkg.org/badges/version/", packagename, "?color=brightgreen");
cranLastMonth_badge <-
  paste0("https://cranlogs.r-pkg.org/badges/last-month/", packagename, "?color=brightgreen");
cranTotal_badge <-
  paste0("https://cranlogs.r-pkg.org/badges/grand-total/", packagename, "?color=brightgreen");

pkgdown_url_dev <-
  paste0("https://r-packages.gitlab.io/", packagename);

pkgdown_url_prod <-
  paste0("https://", packagename, ".opens.science");

```

# <img src='man/figures/logo.png' align="right" style="height:250px" /> `r paste(packagename, "\U1F4E6")`
## `r packageSubtitle`

<!-- badges: start -->

[![CRAN status](`r cran_badge`)](`r cran_url`)

[![Dependency status](`r dependency_badge`)](`r dependency_url`)

[![Pipeline status](`r gitLab_ci_badge`)](`r gitLab_ci_url`)

[![Downloads last month](`r cranLastMonth_badge`)](`r cran_url`)

[![Total downloads](`r cranTotal_badge`)](`r cran_url`)

[![Coverage status](`r codecov_badge`)](`r codecov_url`)

<!-- badges: end -->

The pkgdown website for this project is located at `r pkgdown_url_prod`. If the development version also has a pkgdown website, that's located at `r pkgdown_url_dev`.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->



<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of ``r packagename`` from [CRAN](https://CRAN.R-project.org) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
install.packages('", packagename, "');
```"));
```

You can install the development version of ``r packagename`` from [GitLab](https://about.gitlab.com) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
remotes::install_gitlab('r-packages/", packagename, "');
```"));
```

(assuming you have `remotes` installed; otherwise, install that first using the `install.packages` function)

You can install the cutting edge development version (own risk, don't try this at home, etc)  of ``r packagename`` from [GitLab](https://about.gitlab.com) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
remotes::install_gitlab('r-packages/", packagename, "@dev');
```"));
```

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->


<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
